class Proficiency < ApplicationRecord
	belongs_to :user
	belongs_to :language

	validates :level, numericality: :integer
end
