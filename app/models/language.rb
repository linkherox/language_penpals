class Language < ApplicationRecord
	has_many :messages, dependent: :destroy
	has_many :proficiencies, dependent: :destroy
end
