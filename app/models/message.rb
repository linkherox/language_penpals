class Message < ApplicationRecord
	def users_can_communicate
		lang_id = language_id
		sender_prof = sender.proficiencies.find_by(language_id: lang_id).level
		receiver_prof = receiver.proficiencies.find_by(language_id: lang_id).level

		if (sender_prof - receiver_prof).abs > 2
			errors.add(sender.first_name, "cannot send a message to someone more than 2 values from your proficiency level.")
		end
	end

	validate :users_can_communicate

	belongs_to :sender, class_name: "User"
	belongs_to :receiver, class_name: "User"
	belongs_to :language

end
