module Response

	def json_response(object, status = :ok)

		render json: object, status: status

	end



	def avg_proficiency(proficiencies)
		total_value = 0.0
		length = 0
		proficiencies.each do |proficiency|
			if proficiency.language == @language
				total_value += proficiency.proficiency
				length += 1
			end
		end
		return total_value / length

	end


end