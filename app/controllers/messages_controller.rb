class MessagesController < ApplicationController
	def index
		@messages = Message.all
		json_response(@messages)
	end

	def create
		@message = Message.create(message_params)
		json_response(@message)
	end
	def show
		@message = Message.find(params[:id])
		json_response(@message)
	end
	def update
		@message = Message.find(params[:id])
		@message.update(message_params)
		head :no_content
	end
	def destroy
		@message = Message.find(params[:id])
		@message.destroy
		head :no_content
	end

	private
		def message_params
			params.require(:message).permit(:body, :language_id, :sender_id, :receiver_id)
		end
end
