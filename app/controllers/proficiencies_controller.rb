class ProficienciesController < ApplicationController

	def index
		json_response(@proficiencies)
	end

	def create
		@proficiency = Proficiency.create(proficiency_params)
		lang_id = proficiency_params[:language_id]
		@language = Language.find(lang_id)
		json_response(@proficiency)
	end
	
	def show
		@proficiency = Proficiency.find(params[:id])
		json_response(@proficiency)
	end
	
	def update
		@proficiency = Proficiency.find(params[:id])
		@proficiency.update(proficiency_params)
		head :no_content
	end

	def destroy
		@proficiency = Proficiency.find(params[:id])
		@proficiency.destroy
		head :no_content
	end

	private
		before_action :collect_proficiencies
		before_action :set_current_language, only: [:update, :destroy]
		after_action :update_average, only: [:create, :update, :destroy]

		def set_current_language
			@language = Proficiency.find(params[:id]).language
		end

		def update_average
			@language.update({average_proficiency: Language.find(@language.id).proficiencies.average(:level).to_f})
		end

		def collect_proficiencies
			@proficiencies = Proficiency.all
		end

		# def avg_proficiency(proficiencies)
		# 	total_value = 0.0
		# 	length = 0
		# 	proficiencies.each do |proficiency|
		# 		if proficiency.language == @language
		# 			total_value += proficiency.proficiency
		# 			length += 1
		# 		end
		# 	end
		# 	return total_value / length

		# end

		def proficiency_params
			params.require(:proficiency).permit(:level, :language_id, :user_id)
		end
end
