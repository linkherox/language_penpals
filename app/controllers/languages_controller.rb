class LanguagesController < ApplicationController
	def index
		@languages = Language.all
		json_response(@languages)
	end

	def create
		@language = Language.create(language_params)
		json_response(@language)
	end
	def show
		@language = Language.find(params[:id])
		json_response(@language)
	end
	def update
		@language = Language.find(params[:id])
		@language.update(language_params)
		head :no_content
	end
	def destroy
		@language = Language.find(params[:id])
		if (@language.messages.empty? && @language.proficiencies.empty?)	
			@language.destroy
		end
		head :no_content
	end

	private
		def language_params
			params.require(:language).permit(:name, :average_proficiency)
		end
end
