Rails.application.routes.draw do

  resources :users do
  	resources :messages, :proficiencies
  end
  
  resources :languages do
  	resources :messages, :proficiencies
  end

  resources :messages, :proficiencies

end
