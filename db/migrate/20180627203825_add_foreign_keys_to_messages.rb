class AddForeignKeysToMessages < ActiveRecord::Migration[5.2]
  def change
  	add_reference :messages, :sender
  	add_reference :messages, :receiver
  	add_reference :messages, :language, foreign_key: true
  end
end
